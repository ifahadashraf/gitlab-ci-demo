const {
  sum,
  prod,
  division,
} = require('../src/math');

test('adds 1 + 2 to equal 3', () => {
  expect(sum(1, 2)).toBe(3);
});

test('prod 1 * 2 to equal 2', () => {
  expect(prod(1, 2)).toBe(2);
});

test('Divison 15 / 3 to equal 5', () => {
  expect(division(15, 3)).toBe(5);
});